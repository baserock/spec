#!/usr/bin/env python
"""Quick Check for Baserock Definitions CI.

Usage:
  quick_check.py <defsdir>

Options:
  -h --help     Show this screen.

"""
import jsonschema
import os
import yaml
from docopt import docopt
from fs import open_fs

dfs = None
sfs = None

def _load_morph(filename):
    with dfs.open(filename) as f:
         return yaml.safe_load(f.read())

def _load_schema(filename):
    with sfs.open(filename) as f:
         return yaml.safe_load(f.read())

if __name__ == '__main__':
    arguments = docopt(__doc__, version='Quick Check for Baserock Spec V1.0')

    sdir = os.path.join(os.path.dirname(__file__), 'schemas')
    sfs = open_fs(sdir)
    dfs = open_fs(arguments['<defsdir>'])
    schemas = dict((os.path.splitext(x)[0][1:], _load_schema(x))
                   for x in sfs.walk.files(filter=['*.json-schema']))

    morphs = dict((x[1:], _load_morph(x))
                  for x in dfs.walk.files(filter=['*.morph']))

    for x, y in morphs.iteritems():
        schema = schemas[y.get('kind', 'chunk')]
        try:
            print "Validating %s\n" % x
            jsonschema.validate(y, schema)
            print "Success\n"
        except:
            print "ERROR:  Failed to validate %s\n" % x 
            raise
